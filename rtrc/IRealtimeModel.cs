namespace RtRC {
    public interface IRealtimeModel {
        byte[] ToBytes();
        IRealtimeModel FromBytes(byte[] bytes);
        int ModelIdentifier();
        int ModelLength();
    }
}