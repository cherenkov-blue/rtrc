using System;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace RtRC {
    public class RealtimeUser : IDisposable {
        private UdpClient udpManager;
        private string destinationIP;
        private int destinationPort;
        private EncryptManager encryptManager;

        /// <summary>
        /// Creates a new realtime user
        /// </summary>
        public RealtimeUser() {
            udpManager = new UdpClient();
            encryptManager = new EncryptManager();
        }

        /// <summary>
        /// Creates a new realtime user with a specific private key
        /// <param name="privateKey">The private key of the user</param>
        public RealtimeUser(byte[] privateKey) {
            udpManager = new UdpClient();
            encryptManager = new EncryptManager(privateKey);
        }

        /// <summary>
        /// Sets the IP and port for the current user
        /// </summary>
        /// <param name="destinationIP">The IP address of the user</param>
        /// <param name="destinationPort">The port of the user</param>
        public void SetEndpoint(string destinationIP, int destinationPort) {
            this.destinationIP = destinationIP;
            this.destinationPort = destinationPort;
        }

        /// <summary>
        /// Sends a UDP datagram
        /// </summary>
        /// <param name="data">The blob of data to send</param>
        public void SendDatagram(byte[] data) {
            udpManager.Send(data, data.Length, destinationIP, destinationPort);
        }

        /// <summary>
        /// Gets the encryption manager for the user
        /// </summary>
        /// <returns>The users encryption manager</returns>
        public EncryptManager GetEncryptManager() => encryptManager;

        /// <summary>
        /// Checks to see if the given auth token is valid for this user
        /// </summary>
        /// <param name="authToken">The auth token</param>
        /// <param name="IV">The AES Initialization Vector</param>
        /// <param name="range">The time in seconds before and after the present time in which the token is valid</param>
        /// <returns></returns>
        public bool IsValidAuthToken(byte[] authToken, byte[] IV, int range = 10) {
            long time = GetCurrentTime();
            long authTime;
            try {
                authTime = BitConverter.ToInt64(encryptManager.Decrypt(authToken, IV));
            }
            catch (CryptographicException) {
                return false;
            }

            if (authTime > time + range || authTime < time - range) return false;

            return true;
        }

        /// <summary>
        /// Gets the current auth token for the user
        /// </summary>
        /// <returns>The current auth token for the user</returns>
        public byte[] GetAuthToken() {
            return GetAuthToken(GetCurrentTime());
        }

        /// <summary>
        /// Gets the auth token for a user at a specific time
        /// </summary>
        /// <param name="time">The time to get the auth token for</param>
        /// <returns>The auth token at the specified time</returns>
        public byte[] GetAuthToken(long time) {
            return encryptManager.Encrypt(BitConverter.GetBytes(time));
        }
        private long GetCurrentTime() {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (long) t.TotalSeconds;
        }
        public void Dispose() {
            encryptManager.Dispose();
            udpManager.Dispose();
        }
    }
}