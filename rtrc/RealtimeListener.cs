using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace RtRC {
    public class RealtimeListener : IDisposable {
        private RealtimeCallbackRegistry callbackRegistry;
        private RealtimeUserDB userDB;
        private UdpClient udpManager;
        private bool isInterrupted;
        private string listenAddress;
        private int listenPort;
        public Queue<int> packetCleanupQueue;
        private Dictionary<int, Dictionary<int, byte[]>> packetOrder;
        public RealtimeListener(string address, int port, RealtimeUserDB userDB, RealtimeCallbackRegistry callbackRegistry) {
            this.userDB = userDB;
            udpManager = new UdpClient(port);
            listenAddress = address;
            listenPort = port;
            this.callbackRegistry = callbackRegistry;

            packetOrder = new Dictionary<int, Dictionary<int, byte[]>>();
            packetCleanupQueue = new Queue<int>();
        }

        /// <summary>
        /// Listen for incoming packets. THIS IS A BLOCKING CALL!
        /// </summary>
        public void Listen() {
            while (isInterrupted == false) {
                // Recieve the packet
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                byte[] packetBytes = udpManager.Receive(ref anyIP);
                
                // Terminate execution if udpManager is interupted before recieving data
                if (isInterrupted) break;

                string ip = anyIP.Address.ToString();
                int port = anyIP.Port; 

                // Validate the length of the packet
                if (packetBytes.Length > RealtimeConstants.MAX_PACKET_BYTES) {
                    Console.WriteLine("Oversized packet recieved from {0}:{1}", ip, port); // TODO log this
                    continue;
                }

                // Authenticate the user
                var authToken = packetBytes.Skip(packetBytes.Length - 16).Take(16).ToArray();
                var IV = packetBytes.Skip(packetBytes.Length - 32).Take(16).ToArray();
                var user = userDB.GetUser(authToken, IV, ip, port);
                user.SetEndpoint(ip, listenPort);

                // Log if the auth is invalid
                if (user == null) {
                    Console.WriteLine("Unauthorized callback request from {0}:{1}", ip, port); // TODO log this
                    continue;
                }

                // Decrypt and unpackage the data from the body
                var packetInfo = RealtimePacket.GetPacketData(DecryptBody(packetBytes, user, IV));

                // If it's a single instance packet, trigger the callbacks and continue
                if (packetInfo.orderIdentifier == -1) {
                    callbackRegistry.TriggerCallbacks(packetInfo.modelIdentifier, packetInfo.blob, user);
                    continue;
                }

                // Create the packet order dictionary if it doesn't already exist
                if (!packetOrder.ContainsKey(packetInfo.packetIdentifier)) {
                    packetOrder[packetInfo.packetIdentifier] = new Dictionary<int, byte[]>();

                    // Register packet in the cleanup queue
                    packetCleanupQueue.Enqueue(packetInfo.packetIdentifier);
                }

                // Add the current packet to the packet order dictionary
                packetOrder[packetInfo.packetIdentifier].Add(packetInfo.orderIdentifier, packetInfo.blob);

                // Destroy the packet information if the packet has too many packet segments
                if (packetOrder[packetInfo.packetIdentifier].Count > RealtimeConstants.MAX_PACKET_SEGMENTS) {
                    Console.WriteLine("Packet with too many segments destroyed from {0}:{1}", ip, port); // TODO log this
                    packetOrder.Remove(packetInfo.packetIdentifier);
                    continue;
                }

                // If the packet has been completely recieved, aggregate the bytes and trigger the callbacks
                if (IsPacketComplete(packetInfo.packetIdentifier)) {
                    callbackRegistry.TriggerCallbacks(packetInfo.modelIdentifier, AggregatePacket(packetInfo.packetIdentifier), user);
                    packetOrder.Remove(packetInfo.packetIdentifier);
                }
            }
        }

        private byte[] DecryptBody(byte[] packetBytes, RealtimeUser user, byte[] IV) {
            byte[] cipherText = packetBytes.Take(packetBytes.Length - 32).ToArray();
            return user.GetEncryptManager().Decrypt(cipherText, IV).ToArray();
        }


        private bool IsPacketComplete(int packetIdentifier) {
            // Get the packet order dictionary
            var packetDict = packetOrder[packetIdentifier];

            // Get the minimum value of the packet
            var min = packetDict.Min(a => a.Key);

            // If the count of packets is equivalent to the negative of the start packet order, the packet is complete
            if (packetDict.Keys.Count == -min) return true;

            // Otherwise the packet is not complete
            return false;
        }
        private byte[] AggregatePacket(int packetIdentifier) {
            // Get the packet order dictionary
            var packetDict = packetOrder[packetIdentifier];

            // Order the packets
            var orderedByteBlobs = packetDict.OrderBy(a => a.Key);

            // Assemble the packet blobs
            List<byte> allBytes = new List<byte>();
            foreach (var blob in orderedByteBlobs) allBytes.AddRange(blob.Value);

            // Return the assembled packet blob
            return allBytes.ToArray();
        }
        private void CleanPacketQueue() {
            if (packetCleanupQueue.Count >= RealtimeConstants.MAX_PACKET_QUEUE) {
                packetOrder.Remove(packetCleanupQueue.Dequeue());
            }
        }
        public void Dispose() {
            isInterrupted = true;
            udpManager.Dispose();
        }
    }
}