using System;
using System.Collections.Generic;

namespace RtRC {
    public class RealtimeUserDB {

        private Dictionary<string, RealtimeUser> userEndpoint;
        private List<RealtimeUser> users;

        public RealtimeUserDB() {
            userEndpoint = new Dictionary<string, RealtimeUser>();
            users = new List<RealtimeUser>();
        }

        /// <summary>
        /// Registers the provided user with the UserDB
        /// </summary>
        /// <param name="newUser">The user to register</param>
        public void RegisterUser(RealtimeUser newUser) {
            users.Add(newUser);
        }

        /// <summary>
        /// Gets a user from an auth token
        /// </summary>
        /// <param name="authToken">The auth token</param>
        /// <param name="IV">The AES Initialization Vector</param>
        /// <param name="ip">(Optional) The IP of the user (for caching)</param>
        /// <param name="port">(Optional) The port of the user (for caching)</param>
        /// <returns>The user associated with the auth token, or null if the auth token is invalid</returns>
        public RealtimeUser GetUser(byte[] authToken, byte[] IV, string ip = "", int port = 0) {
            var ipPort = $"{ip}:{port}";

            if (userEndpoint.ContainsKey(ipPort)) {
                var user = userEndpoint[ipPort];
                if (user.IsValidAuthToken(authToken, IV)) return user;
            }

            foreach (var user in users) {
                if (!user.IsValidAuthToken(authToken, IV)) continue;
                userEndpoint.Add(ipPort, user);
                Console.WriteLine("New connection from {0}", ipPort); // TODO log this
                return user;
            }

            return null;
        }

        public RealtimeUser[] GetAllUsers() => users.ToArray();
    }
}