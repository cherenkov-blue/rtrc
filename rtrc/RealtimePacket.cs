using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace RtRC {
    public class RealtimePacket {
        private byte[] packetBytes;
        private IRealtimeModel model;
        private Guid requestGuid;
        private EncryptManager encryptManager;
        private RealtimeUser user;

        /// <summary>
        /// Create a new RealtimePacket
        /// </summary>
        /// <param name="packetBytes">The bytes of data to send</param>
        /// <param name="model">The format of the data to send</param>
        /// <param name="user">The user to send the data to</param>
        public RealtimePacket(byte[] packetBytes, IRealtimeModel model, RealtimeUser user) {
            this.packetBytes = packetBytes;
            this.model = model;
            this.requestGuid = Guid.NewGuid();
            this.encryptManager = user.GetEncryptManager();
            this.user = user;
        }

        /// <summary>
        /// Create a new RealtimePacket from an array of model objects
        /// </summary>
        /// <param name="models">The array of objects to send</param>
        /// <param name="user">The user to send the objects to</param>
        /// <returns>A new realtime packet based on the specified parameters</returns>
        public static RealtimePacket fromModels(IRealtimeModel[] models, RealtimeUser user) {
            // Packets cannot be formed from an empty model list
            if (models.Length == 0) return null;

            // Assemble the model bytes
            List<byte> allBytes = new List<byte>();
            foreach (var model in models) {
                allBytes.AddRange(model.ToBytes());
            }
            // Return a new packet instance based on the models
            return new RealtimePacket(allBytes.ToArray(), models[0], user);
        }

        /// <summary>
        /// Sends the realtime packet
        /// </summary>
        public void Send() {
            int total_packets = (int) Math.Ceiling(((float) packetBytes.Length / RealtimeConstants.MAX_MODEL_LENGTH));
            for (int i = 0; i < total_packets; i++) {

                // If the packet is the first packet being sent, set the packet order ID to negative to identify it as such
                // The value that it is negative as will be the total number of packets that will be sent
                var packetOrderID = i;
                if (i == 0) packetOrderID = -total_packets;

                byte[] IV = encryptManager.RollIV();
                // Encrypted data 
                List<byte> thisPacketBytes = packetBytes.Skip(i * RealtimeConstants.MAX_MODEL_LENGTH).Take(RealtimeConstants.MAX_MODEL_LENGTH).ToList();
                thisPacketBytes.AddRange(BitConverter.GetBytes(model.ModelIdentifier()));
                thisPacketBytes.AddRange(requestGuid.ToByteArray().Take(4));
                thisPacketBytes.AddRange(BitConverter.GetBytes(packetOrderID));
                thisPacketBytes = new List<byte>(encryptManager.Encrypt(thisPacketBytes.ToArray()));

                // Add the initialization vector
                thisPacketBytes.AddRange(IV);

                // Add auth token
                thisPacketBytes.AddRange(user.GetAuthToken());

                // Send the datagram
                user.SendDatagram(thisPacketBytes.ToArray());
            }
        }

        /// <summary>
        /// Sends the realtime packet asynchronously
        /// </summary>
        public void SendAsync() {
            Task.Factory.StartNew(() => Send());
        }

        /// <summary>
        /// A static method for extracting packet information from a decrypted blob
        /// </summary>
        /// <param name="decryptedBlob">The byte[] holding obtained from decrypting a realtime packet</param>
        /// <returns>A tuple with packet information</returns>
        public static(int orderIdentifier, int packetIdentifier, int modelIdentifier, byte[] blob) GetPacketData(byte[] decryptedBlob) {
            int packetOrder = BitConverter.ToInt32(decryptedBlob.Skip(decryptedBlob.Length - 4).Take(4).ToArray());
            int packetIdentifier = BitConverter.ToInt32(decryptedBlob.Skip(decryptedBlob.Length - 8).Take(4).ToArray());
            int modelIdentifier = BitConverter.ToInt32(decryptedBlob.Skip(decryptedBlob.Length - 12).Take(4).ToArray());
            byte[] dataBlob = decryptedBlob.Take(decryptedBlob.Length - 12).ToArray();

            return (packetOrder, packetIdentifier, modelIdentifier, dataBlob);
        }
    }
}