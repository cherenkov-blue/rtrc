using System;
using System.IO;
using System.Security.Cryptography;

namespace RtRC {
    public class EncryptManager : IDisposable {
        private Aes encryptManager;

        /// <summary>
        /// Generate a new encryption manager with a random private key
        /// </summary>
        public EncryptManager() {
            encryptManager = Aes.Create();
            encryptManager.Padding = PaddingMode.PKCS7;
            Console.WriteLine("Private Key: {0}", Convert.ToBase64String(encryptManager.Key));
        }

        /// <summary>
        /// Generate a new encryption maanger with the specified private key
        /// </summary>
        /// <param name="privateKey">The private key of the encryption manager</param>
        public EncryptManager(byte[] privateKey) {
            encryptManager = Aes.Create();
            encryptManager.Key = privateKey;
        }

        /// <summary>
        /// Encrypt some data
        /// </summary>
        /// <param name="data">The plaintext data to encrypt</param>
        /// <returns>The ciphertext created from the provided data</returns>
        public byte[] Encrypt(byte[] data) {
            // Refuse empty data
            if (data.Length <= 0) throw new ArgumentNullException("data");

            using(var encryptor = encryptManager.CreateEncryptor(encryptManager.Key, encryptManager.IV)) {
                return PerformCryptography(encryptor, data);
            }
        }

        /// <summary>
        /// Decrypt some data
        /// </summary>
        /// <param name="cipherText">The provided ciphertext</param>
        /// <param name="IV">The CBC initialization vector</param>
        /// <returns>The plaintext data</returns>
        public byte[] Decrypt(byte[] cipherText, byte[] IV) {
            encryptManager.IV = IV;
            if (cipherText.Length <= 0) throw new ArgumentNullException("cipherText");

            using(var decryptor = encryptManager.CreateDecryptor(encryptManager.Key, encryptManager.IV)) {
                return PerformCryptography(decryptor, cipherText);
            }
        }
        private byte[] PerformCryptography(ICryptoTransform cryptoTransform, byte[] data) {
            using(var memoryStream = new MemoryStream()) {
                using(var cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write)) {
                    cryptoStream.Write(data, 0, data.Length);
                    cryptoStream.FlushFinalBlock();
                    return memoryStream.ToArray();
                }
            }
        }
        public byte[] RollIV() {
            encryptManager.GenerateIV();
            return encryptManager.IV;
        }
        public void Dispose() {
            encryptManager.Dispose();
        }
    }
}