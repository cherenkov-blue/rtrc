namespace RtRC {
    public static class RealtimeConstants {
        public const int MAX_MODEL_LENGTH = 448;
        public const int MAX_PACKET_BYTES = 496;
        public const int MAX_PACKET_SEGMENTS = 32;
        public const int MAX_PACKET_QUEUE = 5000;
    }
}